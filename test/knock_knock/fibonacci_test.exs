defmodule KnockKnock.FibonacciTest do
  use ExUnit.Case
  import KnockKnock.Fibonacci, only: [fib: 1]
  doctest KnockKnock.Fibonacci

  describe "fibonacci" do
    test "zero" do
      assert fib(0) == 0
    end

    test "one" do
      assert fib(1) == 1
    end

    test "a small number" do
      assert fib(10) == 55
    end

    test "a very large number" do
      assert fib(1234) == fib(1233) + fib(1232)
    end

    test "negative inputs not supported" do
      assert_raise FunctionClauseError, fn -> fib(-12) end
    end
  end
end
