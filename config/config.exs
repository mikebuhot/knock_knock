# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :knock_knock, KnockKnockWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Zr8RP9OX7YroB6OeeT19aRTNYefR1Q0SoMIDzvXYrnp23BFP+sf3Dp9fv4/uIax0",
  render_errors: [view: KnockKnockWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: KnockKnock.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
