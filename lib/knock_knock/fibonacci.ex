defmodule KnockKnock.Fibonacci do

  @doc """
  Computes the nth fibonacci number.

  ## Examples

      iex> KnockKnock.Fibonacci.fib(5)
      5

      iex> KnockKnock.Fibonacci.fib(50)
      12586269025
  """
  @spec fib(non_neg_integer) :: non_neg_integer
  def fib(0), do: 0
  def fib(1), do: 1
  def fib(n) when (n > 1) do
    (2..n)
    |> Enum.reduce({0, 1}, fn _, {a, b} -> {b, a+b} end)
    |> elem(1)
  end
end
