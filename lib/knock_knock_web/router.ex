defmodule KnockKnockWeb.Router do
  use KnockKnockWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", KnockKnockWeb do
    pipe_through :api
  end
end
